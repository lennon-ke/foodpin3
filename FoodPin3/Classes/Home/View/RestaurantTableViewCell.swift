//
//  RestaurantTableViewCell.swift
//  FoodPin3
//
//  Created by 柯秉钧 on 16/9/15.
//  Copyright © 2016年 柯秉钧. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var thumbnailImageView: UIImageView!

    var restaurant:Restaurant!{
        didSet{
            nameLabel.text = restaurant.name
            locationLabel.text = restaurant.location
            typeLabel.text = restaurant.type
            thumbnailImageView.image = UIImage(data: restaurant.image as! Data)
        }
    }
    override func awakeFromNib() {

        super.awakeFromNib()
   
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
