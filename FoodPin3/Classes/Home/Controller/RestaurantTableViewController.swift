//
//  RestaurantTableViewController.swift
//  FoodPin3
//
//  Created by 柯秉钧 on 16/9/15.
//  Copyright © 2016年 柯秉钧. All rights reserved.
//

import UIKit
import CoreData
private let cellIdentifer = "Cell"
class RestaurantTableViewController: UITableViewController {
    var restaurants:[Restaurant] = []
    var fetchResultController:NSFetchedResultsController<Restaurant>!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        tableView.estimatedRowHeight = 80.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
     
        fetchData()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = true
    }
    
    
}
// MARK: - 方法
extension RestaurantTableViewController{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRestaurantDetail" {
            if let index = tableView.indexPathForSelectedRow{
                let devc = segue.destination as! RestaurantDetailViewController
                devc.restaurant  = restaurants[index.row]
            }
        }
    }
    // MARK: - 获取coredate对象
    func fetchData(){
        
        let fetchRequest = NSFetchRequest<Restaurant>(entityName: "Restaurant")
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            fetchResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            fetchResultController.delegate = self
            do{
                try fetchResultController.performFetch()
                restaurants = fetchResultController.fetchedObjects as [Restaurant]!
            }catch{
                print(error)
            }
        }
        
    }
}
//MARK: -数据源方法
extension RestaurantTableViewController{
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer, for: indexPath) as! RestaurantTableViewCell
        cell.restaurant = restaurants[indexPath.row]
        return cell
    }
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let shareAction = UITableViewRowAction(style: .default, title: "share") { (action, indexPath) in
            let defaultText = "Just checking in at " + self.restaurants[indexPath.row].name
            
            if let imageToShare = UIImage(data: self.restaurants[indexPath.row].image as! Data){
                let activityController = UIActivityViewController(activityItems: [defaultText,imageToShare], applicationActivities: nil)
                self.present(activityController, animated: true, completion: nil)
            }
        }
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
//            self.restaurants.remove(at: indexPath.row)
//            self.tableView.deleteRows(at: [indexPath], with: .fade)
            if let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
                let restaurantToDelete = self.fetchResultController.object(at: indexPath) 
                context.delete(restaurantToDelete)
                do{
                    try context.save()
                }catch{
                    print(error)
                }
                
            }
            
        }
        shareAction.backgroundColor = UIColor(red: 28/255, green: 165/255, blue: 253/255, alpha: 1)
        deleteAction.backgroundColor = UIColor(red: 202/255, green: 202/255, blue: 203/255, alpha: 1)
        return [deleteAction,shareAction]
    }


}

//MARK: -代理方法
extension RestaurantTableViewController:NSFetchedResultsControllerDelegate{
    
    /**
     override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     tableView.deselectRow(at: indexPath , animated: true)
     //创建一个可选的弹出菜单
     let optionMenu = UIAlertController(title: nil, message: "what do you want to do", preferredStyle: .actionSheet)
     // add action to the menu
     let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
     
     let callActionHandler = { (action:UIAlertAction) in
     let alertMessage = UIAlertController(title: "Service Unavailable", message: "Sorry, the call feature is not available yet. Please retry later.", preferredStyle: .alert)
     alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
     self.present(alertMessage, animated: true, completion: nil)
     }
     let callAction = UIAlertAction(title: "Call " + "123-000-\(indexPath.row)", style: .default, handler: callActionHandler)
     
     let isVisitedAction = UIAlertAction(title: "I Have been here", style: .default) { (action:UIAlertAction) in
     let cell = tableView.cellForRow(at: indexPath)
     cell?.accessoryType = .checkmark
     self.restaurantIsVisited[indexPath.row] = true
     }
     optionMenu.addAction(cancelAction)
     optionMenu.addAction(callAction)
     optionMenu.addAction(isVisitedAction)
     self.present(optionMenu, animated: true, completion: nil)
     }
     */
    // MARK: -处理coredata数据
    @IBAction func unwindToHomeScreen(segue:UIStoryboardSegue){
        
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let _newIndexPath = newIndexPath {
                tableView.insertRows(at: [_newIndexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath{
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath{
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
        default:
            tableView.reloadData()
        }
        restaurants = controller.fetchedObjects as! [Restaurant]
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}















