//
//  Restaurant+CoreDataProperties.swift
//  FoodPin3
//
//  Created by 柯秉钧 on 2016/9/21.
//  Copyright © 2016年 柯秉钧. All rights reserved.
//

import Foundation
import CoreData


extension Restaurant {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Restaurant> {
        return NSFetchRequest<Restaurant>(entityName: "Restaurant");
    }

    @NSManaged public var name: String
    @NSManaged public var type: String
    @NSManaged public var location: String
    @NSManaged public var phoneNumber: String?
    @NSManaged public var isVisited: Bool
    @NSManaged public var rating: String?
    @NSManaged public var image: NSData?

}
